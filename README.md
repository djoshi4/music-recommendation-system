# Music Recommendation System #

o	Developed a music recommendation system using user based and item based collaborative filtering techniques.

o	Implemented and improvised the Similarity Based Neighborhood Method algorithm to make it linearly scalable to the number of users, by using the concept of power users.